#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }


  int lhs_length = strlength(lhs);
  int rhs_length = strlength(rhs);
  int max_length = lhs_length > rhs_length ? lhs_length : rhs_length;

  char *result = calloc(max_length + 2, sizeof(char));
  if (result == NULL) {
    debug_abort("Memory allocation failed");
  }

  int carry = 0;
  for (int i = 0; i < max_length; ++i) {
    int lhs_digit = i < lhs_length ? get_digit_value(lhs[lhs_length - i - 1]) : 0;
    int rhs_digit = i < rhs_length ? get_digit_value(rhs[rhs_length - i - 1]) : 0;

    if (VERBOSE) {
      fprintf(stderr, "add: digit %d digit %d carry %d\n", lhs_digit, rhs_digit, carry);
    }

    int sum = lhs_digit + rhs_digit + carry;
    result[i] = to_digit(sum % base);
    carry = sum / base;

    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %d carry %d\n", sum % base, carry);
    }

  }

  if (carry != 0) 
  {
    if (VERBOSE) {
      fprintf(stderr, "add: final carry %d\n", carry);
    }
    result[max_length] = to_digit(carry);
  }

  return drop_leading_zeros(reverse(result));
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  int lhs_length = strlength(lhs);
  int rhs_length = strlength(rhs);

  char *result = calloc(lhs_length + 1, sizeof(char));
  if (result == NULL) {
    debug_abort("Memory allocation failed");
  }
  int borrow = 0;
  for (int i = 0; i < lhs_length; ++i) 
  {

    int lhs_digit = get_digit_value(lhs[lhs_length - i - 1]);
    int rhs_digit = i < rhs_length ? get_digit_value(rhs[rhs_length - i - 1]) : 0;

    if (VERBOSE) {
      fprintf(stderr, "sub: digit %d digit %d carry %d\n", lhs_digit, rhs_digit, borrow);
    }

    rhs_digit += borrow;
    if (lhs_digit < rhs_digit) 
    {
      lhs_digit += base;
      borrow = 1;
      if (i+1 == lhs_length)
      {
        return NULL;
      }
    } 
    else 
    {
      borrow = 0;
    }

    result[i] = to_digit(lhs_digit - rhs_digit);
    if (VERBOSE) {
      fprintf(stderr, "sub: result: digit %d carry %d\n", lhs_digit - rhs_digit, borrow);
    }
  }


  return drop_leading_zeros(reverse(result));
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  int lhs_length = strlength(lhs);
  int rhs_length = strlength(rhs);

  char *result = calloc(lhs_length + rhs_length + 1, sizeof(char));
  if (result == NULL) {
    debug_abort("Memory allocation failed");
  }

  for (int i = 0; i < lhs_length; ++i) 
  {
    int carry = 0;
    int lhs_digit = get_digit_value(lhs[lhs_length - i - 1]);

    for (int j = 0; j < rhs_length || carry != 0; ++j) 
    {
      int rhs_digit = j < rhs_length ? get_digit_value(rhs[rhs_length - j - 1]) : 0;

      if (VERBOSE) {
        fprintf(stderr, "mul: digit %d digit %d carry %d\n", lhs_digit, rhs_digit, carry);
      }
      int sum = lhs_digit * rhs_digit + carry;
      if (result[i + j] != 0)
      {
        sum += get_digit_value(result[i + j]);
      }
      result[i + j] = to_digit(sum % base);
      carry = sum / base;
      if (VERBOSE) {
        fprintf(stderr, "mul: result: digit %d carry %d\n", sum % base, carry);
      }
    }
  }
  return drop_leading_zeros(reverse(result));

}


unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlength(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}