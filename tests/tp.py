# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    a = '0'
    b = 'S'
    for i in range (n):
        a = b + a
    return(a)

def S(n: str) -> str:
    return('S' + n)

def addition(a: str, b: str) -> str:
    n = '0'
    for i in range(len(a) + len(b) - 2):
        n = 'S' + n
    return(n)

def multiplication(a: str, b: str) -> str:
    n = '0'
    for i in range ((len(a) - 1) * (len(b) - 1)):
        n = 'S' + n
    return n     

def facto_ite(n: int) -> int:
    a = 1
    for i in range(n):
        a = a * (i + 1)
    return a

def facto_rec(n: int) -> int:
    if n <= 1:
        return(1)
    return(facto_rec(n-1) * n)

def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n <= 2:
        return 1
    return(fibo_rec(n - 2) + fibo_rec(n - 1))

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n <= 2:
        return 1
    l = [0, 1, 1]
    for i in range(2, n):
        l.append(l[i - 1] + l[i])
    return l[n]

def golden_phi(n: int) -> int:
    if n == 0:
        return 1
    else:
        return 1 + 1 / golden_phi(n-1)


def sqrt5(n: int) -> int:
    return((golden_phi(n) * 2) - 1)

def pow(a: float, n: int) -> float:
    x = 1
    for i in range(n):
        x = x * a 
    return x

